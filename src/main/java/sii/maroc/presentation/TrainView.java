package sii.maroc.presentation;

import sii.maroc.train.Wagon.WagonType;

public interface TrainView {

	void write(final WagonType wagon);
	
}
