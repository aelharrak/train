package sii.maroc.presentation;

import java.util.HashMap;
import java.util.Map;

import sii.maroc.train.Wagon.WagonType;

public class TrainViewImpl implements TrainView {

	private StringBuilder trainView;
	private Map<WagonType, String> assignAbreviationToWagon;
	private final String LINK_ATTACHEMENT = "::";

	public TrainViewImpl() {
		trainView = new StringBuilder();
		assignAbreviationToWagon = new HashMap<>();
		assignAbreviationToWagon.put(WagonType.HEAD_LEFT, "<HHHH");
		assignAbreviationToWagon.put(WagonType.PASSENGER, "|OOOO|");
		assignAbreviationToWagon.put(WagonType.RESTAURANT, "|hThT|");
		assignAbreviationToWagon.put(WagonType.CARGO, "|____|");
		assignAbreviationToWagon.put(WagonType.CARGO_FILLED, "|^^^^|");
		assignAbreviationToWagon.put(WagonType.HEAD_RIGTH, "HHHH>");
	}

	public String getTrainView() {
		final String trainViewResult = trainView.toString();
		return trainViewResult.replaceAll(LINK_ATTACHEMENT + "$", "");
	}

	@Override
	public void write(WagonType wagon) {
		final String currentWagon = assignAbreviationToWagon.get(wagon);
		trainView.append(currentWagon);
		trainView.append(LINK_ATTACHEMENT);
	}
}
