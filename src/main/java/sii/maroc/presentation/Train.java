package sii.maroc.presentation;

import sii.maroc.train.Wagons;

public class Train {
	
	private Wagons wagons;
	private TrainView trainView;

	public Train(final String train) {
		this.wagons = new Wagons(train);
	}

	public void detachEnd() {
		wagons.detachEnd();
	}

	public void detachHead() {
		wagons.detachHead();
	}

	public boolean fill() {
		return wagons.fill();
	}

	public String print() {
		trainView = new TrainViewImpl();
		wagons.write(trainView);
		return ((TrainViewImpl)trainView).getTrainView();
	}

}
