package sii.maroc.train;

import java.util.LinkedList;

import sii.maroc.presentation.TrainView;

public class Wagons {

	private LinkedList<Wagon> wagons;
	private TrainFactory trainFactory;

	public Wagons(final String train) {
		this.trainFactory = TrainFactory.getInstance();
		wagons = trainFactory.createTrain(train);
	}

	public void detachEnd() {
		wagons.removeLast();
	}

	public void detachHead() {
		wagons.removeFirst();
	}

	public boolean fill() {
		for (Wagon wagon : wagons)
			if (wagon.canBeFill())
				return wagon.fill();
		return false;
	}

	public void write(TrainView trainView) {
		for (Wagon wagon : wagons) {
			trainView.write(wagon.getWagonType());
		}
	}

}
