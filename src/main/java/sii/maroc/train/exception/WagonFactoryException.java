package sii.maroc.train.exception;

@SuppressWarnings("serial")
public class WagonFactoryException extends RuntimeException {

	public WagonFactoryException() {
		super();
	}

	public WagonFactoryException(final String message) {
		super(message);
	}

}
