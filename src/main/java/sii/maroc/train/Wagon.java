package sii.maroc.train;

public class Wagon {

	public enum WagonType {
		HEAD_LEFT, PASSENGER, RESTAURANT, CARGO, CARGO_FILLED, HEAD_RIGTH;
	}

	private WagonType wagonType;

	public Wagon(final WagonType wagonType) {
		this.wagonType = wagonType;
	}

	public WagonType getWagonType() {
		return wagonType;
	}

	public boolean fill() {
		wagonType = WagonType.CARGO_FILLED;
		return true;
	}

	public boolean canBeFill() {
		return wagonType == WagonType.CARGO;
	}

}
