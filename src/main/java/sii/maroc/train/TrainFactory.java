package sii.maroc.train;

import java.util.LinkedList;

import sii.maroc.train.Wagon.WagonType;
import sii.maroc.train.exception.WagonFactoryException;

class TrainFactory {

	private static TrainFactory instance = new TrainFactory();

	private TrainFactory() {}

	public static TrainFactory getInstance() {
		return instance;
	}

	public LinkedList<Wagon> createTrain(String train) {
		train = train.replaceAll("H$", "E");
		LinkedList<Wagon> wagons = new LinkedList<>();
		for (int abreviationOfTrain = 0; abreviationOfTrain < train.length(); abreviationOfTrain++) {
			final Wagon wagon = createWagon(train, abreviationOfTrain);
			wagons.add(wagon);
		}
		return wagons;
	}

	private Wagon createWagon(final String train, final int abreviationOfTrain) {
		final char wagonAbreviation = train.charAt(abreviationOfTrain);
		switch (wagonAbreviation) {
		case 'H': return new Wagon(WagonType.HEAD_LEFT);
		case 'E': return new Wagon(WagonType.HEAD_RIGTH);
		case 'P': return new Wagon(WagonType.PASSENGER);
		case 'R': return new Wagon(WagonType.RESTAURANT);
		case 'C': return new Wagon(WagonType.CARGO);
		default: throw new WagonFactoryException("Impossible to create a " + wagonAbreviation);
		}
	}
}
